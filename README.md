# secretEscapes

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.15.1.

## Prerequisites
1. Install node js `https://nodejs.org/en/download/`
2. Install dependencies `npm install -g grunt-cli bower yo generator-angular`

## Build & development
1. Clone git repo `git@bitbucket.org:OmarCreativeDev/secretescapestest.git` secretEscapes
2. Navigate to repo `cd secretEscapes`
3. run `npm install`
4. run `bower install`
5. Run `grunt serve` to preview