'use strict';

secretEscapesApp.controller('mainCtrl', ['mainService', '$q', function(mainService, $q) {
	var mainCtrl = this;

	mainCtrl.loadingData = true;

	mainService.getTopStoryIDs().then(function(response){
		mainCtrl.topStoryIDs = response.data;
		console.log(response.data);

		// store promises in array
		var storyPromises = [];

		// loop over top story ID's and add promise to array
		for(var i = 0; i < mainCtrl.topStoryIDs.length; i++){
			storyPromises.push(mainService.getStories(mainCtrl.topStoryIDs[i]));
		}

		$q.all(storyPromises).then(function(response){
			console.log('%c all promises resolved', 'background:green; color:#fff');
			mainCtrl.stories = [];

			for(var i = 0; i < response.length; i++){
				console.log(response[i].data);
				mainCtrl.stories.push(response[i].data);
			}

			mainCtrl.loadingData = false;
		});
	});
}]);