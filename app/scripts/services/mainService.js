'use strict';

secretEscapesApp.service('mainService', ['$http', '$q', function($http, $q) {

	console.log('mainService loaded');

	var mainService = this;

	mainService.getTopStoryIDs = function() {
		console.log('mainService.getTopStoryIDs() called');

		var deferred = $q.defer();

		$http.get('https://hacker-news.firebaseio.com/v0/topstories.json', { cache: 'true' })
			.then(function (response) {
				deferred.resolve(response);
			});

		return deferred.promise;
	};

	mainService.getStories = function(storyID) {
		console.log('mainService.getStories() called with ID = ' + storyID);

		var deferred = $q.defer();

		$http.get('https://hacker-news.firebaseio.com/v0/item/' +storyID+ '.json', { cache: 'true' })
			.then(function (response) {
				deferred.resolve(response);
			});

		return deferred.promise;
	};
}]);