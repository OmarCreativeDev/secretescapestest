secretEscapesApp.directive('storyOpener', function() {
	'use strict';
	return {
		restrict: 'A', //E = element, A = attribute, C = class, M = comment
		link: function (scope, element) {
			element.click(function(){
				if(!element.hasClass('active')){
					var defaultHeight = element.outerHeight(true),
						defaultWidth = element.outerWidth(true);

					element.addClass('active');

					element.css({
						'height': defaultHeight+defaultHeight-20,
						'width': defaultWidth+defaultWidth-10
					});
				} else {
					element.removeAttr('style');
					element.removeClass('active');
				}
			});
		}
	};
});