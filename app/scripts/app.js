'use strict';

/**
 * @ngdoc overview
 * @name secretEscapesApp
 * @description
 * # secretEscapesApp
 *
 * Main module of the application.
 */
var secretEscapesApp = angular.module('secretEscapesApp', ['ngAnimate','ngRoute']);

secretEscapesApp.config(function($routeProvider) {
	$routeProvider
		.when('/', {
			templateUrl: 'views/main.html',
		})
		.otherwise({
			redirectTo: '/'
		});
});